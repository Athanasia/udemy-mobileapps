import 'dart:developer';
import 'dart:math';
import 'package:flutter/material.dart';

void main() => runApp(
      MaterialApp(
        home: BallPage(),
      ),
    );

class BallPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return
    Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue.shade900,
        title: Center(
          child: Text(
              'Ask Me Anything'
          ),
        ),
      ),
      backgroundColor: Colors.blue,
      body: Ball(),
    );
  }

}

class Ball extends StatefulWidget{
  @override
  BallState createState() => BallState();
}

class BallState extends State<Ball>{
  int ballNumber = 0 ;
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: <Widget>[
          Expanded(
            child: FlatButton(
              onPressed: (){
                shakeBall();
              },
              child: Image.asset('images/ball$ballNumber.png'),
            ),

          ),
        ],
      ),
    );
  }

  shakeBall(){
    setState(() {
      ballNumber = Random().nextInt(5) + 1;

    });
  }
}