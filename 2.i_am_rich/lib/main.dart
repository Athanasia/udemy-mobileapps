import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text('Hello Billie man!'),
          ),
          backgroundColor: Colors.blueGrey[900],
        ),
        backgroundColor: Colors.blueGrey,
        body: Center(
          child: Image(
//            image: NetworkImage('https://lh3.googleusercontent.com/KvqPYGh3AfDUWGM6BYOCPfk_JUJjDUkchJ_tr5-a96JqN_BvHbWyAP1zLxDNoxO2j3T2'),
            image: AssetImage('images/diamond.png'),
          ),
        ),
      ),
    ),
  );
}
